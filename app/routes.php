<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'StoreController@getIndex');
//
Route::get('items/{parent_id}', 'StoreController@getItems');
Route::get('itemsDetil/c/{category_id}/p/{parent_id}', 'StoreController@getItemsDetil');
Route::get('items/view/{id}', 'StoreController@getItemsView');
Route::get('{product}', 'StoreController@getItemsView');
//Route::get('{category}');
// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
//

Route::get('store/cart', 'StoreController@getCart');
Route::post('store/addtocart', 'StoreController@postAddtocart');
Route::post('store/delcart', 'StoreController@delCart');

Route::get('/start', function()
{
    $admin = new Role();
    $admin->name = 'Admin';
    $admin->save();
  
    $user = new Role();
    $user->name = 'User';
    $user->save();
  
    $read = new Permission();
    $read->name = 'can_read';
    $read->display_name = 'Can Read Data';
    $read->save();
  
    $edit = new Permission();
    $edit->name = 'can_edit';
    $edit->display_name = 'Can Edit Data';
    $edit->save();
  
    $user->attachPermission($read);
    $admin->attachPermission($read);
    $admin->attachPermission($edit);
 
    $adminRole = DB::table('roles')->where('name', '=', 'Admin')->pluck('id');
    $userRole = DB::table('roles')->where('name', '=', 'User')->pluck('id');
    // print_r($userRole);
    // die();
  
    $user1 = User::where('username','=','admin')->first();
    $user1->roles()->attach($adminRole);
    
    return 'Woohoo!';
});

Route::group(array('before' => 'auth'), function()
{
	Route::group(['before' => 'Admin'], function(){
		Route::get('/dashboard', 'DashboardController@index');
		Route::resource('master/product', 'ProductController');
		Route::get('api/product', array('as'=>'api.product', 'uses'=>'ProductController@getDatatable'));
		Route::resource('master/category', 'CategoryController');
		Route::get('api/category', ['as' => 'api.category', 'uses' => 'CategoryController@getDatatable']);
	});
});

