@extends('frontend.layout')
@section('content')
    <!-- Items -->
    <div class="items">
      <div class="container">
        <div class="row">

          <!-- Sidebar -->
          <div class="col-md-3 col-sm-3 hidden-xs">

            <h5 class="title">Categories</h5>
            <!-- Sidebar navigation -->
              <nav>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display: block; position: static; margin-bottom: 5px; *width: 180px;">
                  <li><a href="index.html">Home</a></li>
                  @foreach($items as $item)
                    <li class="dropdown-submenu">
                        <a href="#" {{ set_active('open') }} data-toggle="dropdown">{{ $item->name }} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                             @foreach($item['children'] as $child)
                                <li class="dropdown-submenu">
                                  <a href="{{ URL::to('itemsDetil/c/'.$child->id.'/p/'.$item->parent_id) }}">{{ $child->name }}</a>
                                  <ul class="dropdown-menu">
                                      @foreach($child['children'] as $child1)
                                          <li class="dropdown-submenu"><a href="{{ URL::to('itemsDetil/c/'.$child->id.'/p/'.$item->parent_id) }}">{{ $child1->name }}</a></li>
                                      @endforeach
                                  </ul>
                                </li>
                             @endforeach
                        </ul>
                    </li>
                 @endforeach
                </ul>
            </nav>
          </div>


    <!-- Main content -->
    <div class="col-md-9 col-sm-9">

    <!-- Breadcrumb -->
    <ul class="breadcrumb">
      <li><a href="index.html">Home</a></li>
      <li><a href="items.html">Smartphone</a></li>
      <li class="active">Apple</li>
    </ul>

        <!-- Title -->
        <h4 class="pull-left"> {{ $category_id[0]['name'] }} </h4>
        <!-- Sorting -->
        <div class="form-group pull-right">
            <select class="form-control">
            <option>Sort By</option>
            <option>Name (A-Z)</option>
            <option>Name (Z-A></option>
            <option>Price (Low-High)</option>
            <option>Price (High-Low)</option>
            <option>Ratings</option>
            </select>
        </div>

      <div class="clearfix"></div>

          <div class="row">
            @foreach($products as $product)
                <div class="col-md-4 col-sm-6">
                  <div class="item">
                      <!-- Item image -->
                      <div class="item-image">
                        <a href="single-item.html">{{ HTML::image('/public/back/'.$product->image->name, null, ['class' => 'img-responsive', 'height' => '100',  'width' => '100']) }}</a>
                      </div>
                      <!-- Item details -->
                        <div class="item-details">
                          <!-- Name -->
                          <!-- Use the span tag with the class "ico" and icon link (hot, sale, deal, new) -->
                          <h5><a href="">{{ $product->name }}</a></h5>
                          <div class="clearfix"></div>
                          <!-- Para. Note more than 2 lines. -->
                          <p>{{ $product->description }}</p>
                          <hr />
                          <!-- Price -->
                          <div class="item-price pull-left">Rp{{ $product->price }}</div>
                          <!-- Add to cart -->
                          <div class="button pull-right"><a href="#">Add to Cart</a></div>
                          <div class="clearfix"></div>
                        </div>
                  </div>
                </div>
            @endforeach


            <div class="col-md-9 col-sm-9">
                <!-- Pagination -->
                <?php echo $products->links(); ?>
            </div>

          </div>


        </div>



        </div>
      </div>
    </div>

@stop