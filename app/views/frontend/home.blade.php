@extends('frontend.layout')
@section('content')
<!-- Flex Slider starts -->
<div class="container flex-main">
  <div class="row">
    <div class="col-md-12">

            <div class="flex-image flexslider">
              <ul class="slides">
                  <!-- Each slide should be enclosed inside li tag. -->

                  <!-- Slide #1 -->
                <li>
                  <!-- Image -->
                  {{ HTML::image('/public/front/img/photos/slider1.jpg', null, ['class' => 'img-responsive']) }}
                  <!-- Caption -->
                  <div class="flex-caption">
                     <!-- Title -->
                     <h3>Levi's T-Shirt - <span class="color">Just $49</span></h3>
                     <!-- Para -->
                     <p>Ut commodo ullamcorper risus nec mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. </p>
                     <div class="button">
                      <a href="single-item.html">Buy Now</a>
                     </div>
                  </div>
                </li>

                  <!-- Slide #2 -->
                <li>
                  {{ HTML::image('/public/front/img/photos/slider2.jpg', null, ['class' => 'img-responsive']) }}
                  <div class="flex-caption">
                     <!-- Title -->
                     <h3>Denim Jeans - <span class="color">Just $149</span></h3>
                     <!-- Para -->
                     <p>Ut commodo ullamcorper risus nec mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. </p>
                     <div class="button">
                      <a href="single-item.html">Buy Now</a>
                     </div>
                  </div>
                </li>

                <li>
                  {{ HTML::image('/public/front/img/photos/slider3.jpg', null, ['class' => 'img-responsive']) }}
                  <div class="flex-caption">
                     <!-- Title -->
                     <h3>Polo Shirts - <span class="color">Just $79</span></h3>
                     <!-- Para -->
                     <p>Ut commodo ullamcorper risus nec mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. </p>
                     <div class="button">
                      <a href="single-item.html">Buy Now</a>
                     </div>
                  </div>
                </li>

                <li>
                  {{ HTML::image('/public/front/img/photos/slider4.jpg', null, ['class' => 'img-responsive']) }}
                  <div class="flex-caption">
                     <!-- Title -->
                     <h3>Raymonds Suitings - <span class="color">Just $449</span></h3>
                     <!-- Para -->
                     <p>Ut commodo ullamcorper risus nec mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. </p>
                     <div class="button">
                      <a href="single-item.html">Buy Now</a>
                     </div>
                  </div>
                </li>

              </ul>
            </div>

    </div>
  </div>
</div>
<!-- Flex slider ends -->

<!-- Promo box starts -->
<div class="promo">
  <div class="container">
    <div class="row">

      <!-- Red color promo box -->
      <div class="col-md-4">
        <!-- rcolor =  Red color -->
        <div class="pbox rcolor">
          <div class="pcol-left">
            <!-- Image -->
            <a href="items.html"><img src="img/photos/promo-1.png" alt="" /></a>
          </div>
          <div class="pcol-right">
            <!-- Title -->
            <p class="pmed"><a href="items.html">HTC One</a></p>
            <!-- Para -->
            <p class="psmall"><a href="items.html">Buy Phones just for $250. Cheap. Dont miss this offer. Keep it checking for more.</a></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>


      <!-- Blue color promo box -->
      <div class="col-md-4">
        <!-- bcolor =  Blue color -->
        <div class="pbox bcolor">
          <div class="pcol-left">
            <a href="items.html"><img src="img/photos/promo-2.png" alt="" /></a>
          </div>
          <div class="pcol-right">
            <p class="pmed"><a href="items.html">Blackberry</a></p>
            <p class="psmall"><a href="items.html">Buy Phones just for $250. Cheap. Dont miss this offer. Keep it checking for more.</a></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

      <!-- Green color promo box -->
      <div class="col-md-4">
        <!-- gcolor =  Green Color -->
        <div class="pbox gcolor">
          <div class="pcol-left">
            <a href="items.html"><img src="img/photos/promo-3.png" alt="" /></a>
          </div>
          <div class="pcol-right">
            <p class="pmed"><a href="items.html">Nokia Lumia</a></p>
            <p class="psmall"><a href="items.html">Buy Phones just for $250. Cheap. Dont miss this offer. Keep it checking for more.</a></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- Promo box ends -->

<!-- Items -->
<div class="items">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title">Popular Deals</h3>
      </div>
      <!-- Item #1 -->
       
      @foreach($product as $product)
        <div class="col-md-3 col-sm-4">
          <div class="item">
              <!-- Item image -->
              <div class="item-image">
                <a href="{{ URL::to($product->slug) }}">{{ HTML::image('/public/back/'.$product->image->name, null, ['class' => 'img-responsive', 'height' => '100',  'width' => '100']) }}</a>
              </div>
              <!-- Item details -->
                <div class="item-details">
                  <!-- Name -->
                  <!-- Use the span tag with the class "ico" and icon link (hot, sale, deal, new) -->
                  <h5><a href="">{{ $product->name }}</a></h5>
                  <div class="clearfix"></div>
                  <!-- Para. Note more than 2 lines. -->
                  <hr />
                  <!-- Price -->
                  <div class="item-price pull-left">Rp{{ $product->price }}</div>
                  <!-- Add to cart -->
                  <div class="pull-right">
                      <!--<a href="#" id="add_cart">Add to Cart</a>-->
                      {{ Form::open(['url' => 'store/addtocart']) }}
                      {{ Form::hidden('quantity', 1) }}
                      {{ Form::hidden('id', $product->id) }}
                      <button type="submit" class="btn btn-info">
                          Add to Cart
                      </button>
                      {{ Form::close() }}
                  </div>
                  <div class="clearfix"></div>
                </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>
<!--/ Items end -->

<!-- Owl Carousel Starts -->
<div class="container">

    <div class="rp">
        <!-- Recent News Starts -->
        <h4 class="title">Recent Items</h4>
        <div class="recent-news block">
                <!-- Recent Item -->
                <div class="recent-item">
                    <div class="custom-nav">
                        <a class="prev"><i class="fa fa-chevron-left br-lblue"></i></a>
                        <a class="next"><i class="fa fa-chevron-right br-lblue"></i></a>
                    </div>
                    <div id="owl-recent" class="owl-carousel">
                        <!-- Item -->
                        <div class="item">
                            <a href="#"><img src="img/photos/4.png" alt="" class="img-responsive" /></a>
                            <!-- Heading -->
                            <h4><a href="#">Sony Xperia <span class="pull-right">$105</span></a></h4>
                            <div class="clearfix"></div>
                            <!-- Paragraph -->
                            <p>Nunc adipiscing, metus sollic itun molestie, urna augue dap ibus dui.</p>
                        </div>
                        <div class="item">
                            <a href="#"><img src="img/photos/2.png" alt="" class="img-responsive" /></a>
                            <!-- Heading -->
                            <h4><a href="#">Applie iPhone <span class="pull-right">$210</span></a></h4>
                            <div class="clearfix"></div>
                            <!-- Paragraph -->
                            <p>Nunc adipiscing, metus sollic itun molestie, urna augue dap ibus dui.</p>
                        </div>
                        <div class="item">
                            <a href="#"><img src="img/photos/3.png" alt="" class="img-responsive" /></a>
                            <!-- Heading -->
                            <h4><a href="#">Google Nexus<span class="pull-right">$310</span></a></h4>
                            <div class="clearfix"></div>
                            <!-- Paragraph -->
                            <p>Nunc adipiscing, metus sollic itun molestie, urna augue dap ibus dui.</p>
                        </div>
                        <div class="item">
                            <a href="#"><img src="img/photos/4.png" alt="" class="img-responsive" /></a>
                            <!-- Heading -->
                            <h4><a href="#">Sony Xperai <span class="pull-right">$10</span></a></h4>
                            <div class="clearfix"></div>
                            <!-- Paragraph -->
                            <p>Nunc adipiscing, metus sollic itun molestie, urna augue dap ibus dui.</p>
                        </div>
                        <div class="item">
                            <a href="#"><img src="img/photos/2.png" alt="" class="img-responsive" /></a>
                            <!-- Heading -->
                            <h4><a href="#">Sony Xperai <span class="pull-right">$10</span></a></h4>
                            <div class="clearfix"></div>
                            <!-- Paragraph -->
                            <p>Nunc adipiscing, metus sollic itun molestie, urna augue dap ibus dui.</p>
                        </div>
                        <div class="item">
                            <a href="#"><img src="img/photos/3.png" alt="" class="img-responsive" /></a>
                            <!-- Heading -->
                            <h4><a href="#">Sony Xperai <span class="pull-right">$10</span></a></h4>
                            <div class="clearfix"></div>
                            <!-- Paragraph -->
                            <p>Nunc adipiscing, metus sollic itun molestie, urna augue dap ibus dui.</p>
                        </div>
                        <div class="item">
                            <a href="#"><img src="img/photos/4.png" alt="" class="img-responsive" /></a>
                            <!-- Heading -->
                            <h4><a href="#">Sony Xperai <span class="pull-right">$10</span></a></h4>
                            <div class="clearfix"></div>
                            <!-- Paragraph -->
                            <p>Nunc adipiscing, metus sollic itun molestie, urna augue dap ibus dui.</p>
                        </div>
                        <div class="item">
                            <a href="#"><img src="img/photos/2.png" alt="" class="img-responsive" /></a>
                            <!-- Heading -->
                            <h4><a href="#">Sony Xperai <span class="pull-right">$10</span></a></h4>
                            <div class="clearfix"></div>
                            <!-- Paragraph -->
                            <p>Nunc adipiscing, metus sollic itun molestie, urna augue dap ibus dui.</p>
                        </div>
                    </div>
                </div>
        </div>

        <!-- Recent News Ends -->
    </div>

</div>
<!-- Owl Carousel Ends -->
@stop