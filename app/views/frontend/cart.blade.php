@extends('frontend.layout')
@section('content')
    <table class="table table-striped tcart">
        <thead>
        <tr>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->qty }}</td>
            <td>Rp{{ number_format($product->price) }}</td>
            <td>
                {{ Form::open(['url' => 'store/delcart']) }}
                    {{ Form::hidden('id', $product->rowid) }}
                    <button class="btn btn-danger">Delete</button>
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
        <tr>
            <th></th>
            <th>Total</th>
            <th>Rp{{ number_format(Cart::total()) }}</th>
        </tr>
        </tbody>
    </table>
@stop