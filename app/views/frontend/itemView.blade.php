@extends('frontend.layout')
@section('content')
<!-- Items -->
<div class="items">
  <div class="container">
    <div class="row">

            <!-- Sidebar -->
      <div class="col-md-3 col-sm-3 hidden-xs">

        <h5 class="title">Categories</h5>
        <!-- Sidebar navigation -->
          <nav>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display: block; position: static; margin-bottom: 5px; *width: 180px;">
              <li><a href="index.html">Home</a></li>
              @foreach($items as $item)
                <li class="dropdown-submenu">
                    <a href="#" {{ set_active('open') }} data-toggle="dropdown">{{ $item->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                         @foreach($item['children'] as $child)
                            <li class="dropdown-submenu">
                              <a href="{{ URL::to('items/'.$child->id) }}">{{ $child->name }}</a>

                            </li>
                         @endforeach
                    </ul>
                </li>
             @endforeach
            </ul>
          </nav>
      </div>

      <!-- Main content -->

      <div class="col-md-9 col-sm-9">

        <!-- Breadcrumbs -->
        <ul class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="items.html">Smartphone</a></li>
          <li class="active">Apple</li>
        </ul>

        <!-- Product details -->

        <div class="product-main">
          <div class="row">
            <div class="col-md-6 col-sm-6">

                <!-- Image. Flex slider -->
                <div class="product-slider">
                  <div class="product-image-slider flexslider">
                    <ul class="slides">
                        <!-- Each slide should be enclosed inside li tag. -->

                        <!-- Slide #1 -->
                      <li>
                        <!-- Image -->
                        {{ HTML::image('/public/back/'.$products->image->name, null, ['class' => 'img-responsive', 'height' => '100',  'width' => '100']) }}
                      </li>
                    </ul>
                  </div>
              </div>



            </div>
            <div class="col-md-6 col-sm-6">
              <!-- Title -->
                <h4 class="title">{{ $products->name }}</h4>
                <h5>Price : Rp{{ number_format($products->price) }}</h5>
                <p>Availability : @if($products->in_stock > 1) In Stock @endif</p>
                  <!-- Dropdown menu -->
                    <div class="form-group">
                        <select class="form-control">
                        <option>White</option>
                        <option>Misty Grey</option>
                        <option>Light Cream</option>
                        <option>Light Blue</option>
                        </select>
                    </div>

                    <!-- Quantity and add to cart button -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                              <input type="text" class="form-control input-sm" />
                              <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" type="button">Add to Cart</button>
                              </span>
                            </div>
                        </div>
                    </div>

                    <!-- Add to wish list -->
                    <a href="wish-list.html">+ Add to Wish List</a>


            </div>
          </div>
        </div>

<br />

        <!-- Description, specs and review -->

        <ul class="nav nav-tabs">
          <!-- Use uniqe name for "href" in below anchor tags -->
          <li class="active"><a href="#tab1" data-toggle="tab">Description</a></li>
          <li><a href="#tab2" data-toggle="tab">Specs</a></li>
          <li><a href="#tab3" data-toggle="tab">Review</a></li>
        </ul>

        <!-- Tab Content -->
        <div class="tab-content">
          <!-- Description -->
          <div class="tab-pane active" id="tab1">
            <h5>{{ $products->name }}</h5>
            <p>{{ $products->description }}</p>
          </div>

          <!-- Sepcs -->
          <div class="tab-pane" id="tab2">

            <h5 class="title">Product Specs</h5>


          </div>

          <!-- Review -->
          <div class="tab-pane" id="tab3">
            <h5>Product Reviews</h5>


            <hr />
            <h5 class="title">Write a Review</h5>

              <div class="form form-small">

                  <!-- Review form (not working)-->
                  <form class="form-horizontal">
                      <!-- Name -->
                      <div class="form-group">
                        <label class="control-label col-md-3" for="name2">Your Name</label>
                        <div class="col-md-6">
                          <input type="text" class="form-control" id="name2">
                        </div>
                      </div>
                      <!-- Select box -->
                      <div class="form-group">
                        <label class="control-label col-md-3">Rating</label>
                        <div class="col-md-6">
                            <select class="form-control">
                            <option>&nbsp;</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select>
                        </div>
                      </div>

                      <!-- Review -->
                      <div class="form-group">
                        <label class="control-label col-md-3" for="name">Your Review</label>
                        <div class="col-md-6">
                          <textarea class="form-control"></textarea>
                        </div>
                      </div>
                      <!-- Buttons -->
                      <div class="form-group">
                         <!-- Buttons -->
                         <div class="col-md-6 col-md-offset-3">
                            <button type="submit" class="btn btn-default">Post</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                      </div>
                  </form>
                </div>

          </div>

        </div>

      </div>



    </div>
  </div>
</div>
@stop