@extends('backend.layout')
@extends('backend.sidebar')
@section('content')
    <!-- Page heading -->
    <div class="page-head">
      <h2 class="pull-left"><i class="fa fa-file-o"></i> Product</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
      <a href="{{ URL::to('dashboard') }}"><i class="fa fa-home"></i> Home</a>
      <!-- Divider -->
      <span class="divider">/</span>
      <a href="#" class="bread-current">Product</a>
    </div>

    <div class="clearfix"></div>

    </div>
    <!-- Page heading ends -->

    <div class="matter">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget wgreen">
                        <div class="widget-head">
                          <div class="pull-left">Forms</div>
                          <div class="widget-icons pull-right">
                            <a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a>
                            <a class="wclose" href="#"><i class="fa fa-times"></i></a>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="padd">
                                <br>
                                {{ Form::open(['route' => 'master.product.store', 'class' => 'form-horizontal', 'files' => 'true']) }}
                                    @include('backend.master.product.form')
                                {{ Form::close() }}
                            </div>
                        </div>
                        <div class="widget-foot"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Matter ends -->
@stop