<div class="form-group">
    {{ Form::label('category_id', 'Category :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">

        <select name="category_id[]" id="category_id" multiple="multiple">
            {{ printTree($buildTree) }}
        </select>
    </div>
</div>
<div class="form-group">
    {{ Form::label('sku', 'SKU :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">
        {{ Form::text('sku', null, ['class' => 'form-control']) }}
        @if ($errors->has('sku')) <p class="help-block" style="color: darkred">{{ $errors->first('sku') }}</p> @endif
    </div>
</div>
<div class="form-group">
    {{ Form::label('name', 'Name :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
        @if ($errors->has('name')) <p class="help-block" style="color: darkred">{{ $errors->first('name') }}</p> @endif
    </div>
</div>
<div class="form-group">
    {{ Form::label('description', 'Description :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">
        {{ Form::textarea('description', null, ['class' => 'form-control', 'size' => '30x5']) }}
        @if ($errors->has('description')) <p class="help-block" style="color: darkred">{{ $errors->first('description') }}</p> @endif
    </div>
</div>
<div class="form-group">
    {{ Form::label('price', 'Price :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">
        {{ Form::text('price', null, ['class' => 'form-control']) }}
        @if ($errors->has('price')) <p class="help-block" style="color: darkred">{{ $errors->first('price') }}</p> @endif
    </div>
</div>
<div class="form-group">
    {{ Form::label('size', 'Size :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">
        {{ Form::text('size', null, ['class' => 'form-control']) }}
        @if ($errors->has('size')) <p class="help-block" style="color: darkred">{{ $errors->first('size') }}</p> @endif
    </div>
</div>
<div class="form-group">
    {{ Form::label('in_stock', 'Stock :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">
        {{ Form::text('in_stock', null, ['class' => 'form-control']) }}
        @if ($errors->has('in_stock')) <p class="help-block" style="color: darkred">{{ $errors->first('in_stock') }}</p> @endif
    </div>
</div>
<div class="form-group">
    {{ Form::label('status', 'Status :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">
        {{ Form::select('status', ['' => 'Select Status','0' => 'unpublish', '1' => 'publish'], null, ['class' => 'form-control'], 'publish') }}
        @if ($errors->has('status')) <p class="help-block" style="color: darkred">{{ $errors->first('status') }}</p> @endif
    </div>
</div>
<div class="form-group">
    {{ Form::label('image_id', 'upload Image :', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-5">
        {{ Form::file('image_id', ['class' => 'form-control']) }}
        @if ($errors->has('image_id')) <p class="help-block" style="color: darkred">{{ $errors->first('image_id') }}</p> @endif
    </div>
</div>
<div class="form-group">
    <div class="col-lg-offset-2 col-lg-6">
        {{ Form::submit('Create', ['class' => 'btn btn-sm btn-primary']) }}
    </div>
</div>
<script>
/* multiple select */
$(document).ready(function(){
    $('#category_id').multipleSelect();
});
</script>