@extends('backend.layout')
@extends('backend.sidebar')
@section('content')
    <!-- Page heading -->
    <div class="page-head">
      <h2 class="pull-left"><i class="fa fa-file-o"></i> Product Category Create</h2>

        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="{{ URL::to('dashboard') }}"><i class="fa fa-home"></i> Home</a>
          <!-- Divider -->
          <span class="divider">/</span>
          <a href="#" class="bread-current">Product Category Create</a>
        </div>

        <div class="clearfix"></div>
    </div>
    <!-- Page heading ends -->

    <div class="matter">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget wgreen">
                        <div class="widget-head">
                          <div class="pull-left">Forms</div>
                          <div class="widget-icons pull-right">
                            <a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a>
                            <a class="wclose" href="#"><i class="fa fa-times"></i></a>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="padd">
                                <br>
                                @if ($errors->any())
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                                {{ Form::open(['route' => 'master.category.store', 'class' => 'form-horizontal', 'files' => 'true']) }}
                                    <div class="form-group">
                                        {{ Form::label('parent_id', 'Parent :', ['class' => 'col-sm-3 control-label']) }}
                                        <div class="col-sm-5">
                                            <select class="form-control" name="parent_id">
                                                <option>Select Parent</option>
                                                {{ printTree($buildTree) }}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('name', 'Name :', ['class' => 'col-sm-3 control-label']) }}
                                        <div class="col-sm-5">
                                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                                            @if ($errors->has('name')) <p class="help-block" style="color: darkred">{{ $errors->first('name') }}</p> @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-2">
                                            {{ Form::submit('Create', ['class' => 'btn btn-primary form-control']) }}
                                        </div>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                        <div class="widget-foot"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Matter ends -->
@stop