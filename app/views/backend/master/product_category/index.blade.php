@extends('backend.layout')
@extends('backend.sidebar')
@section('content')
    <!-- Page heading -->
    <div class="page-head">
      <h2 class="pull-left"><i class="fa fa-file-o"></i> Product Category</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
      <a href="{{ URL::to('dashboard') }}"><i class="fa fa-home"></i> Home</a>
      <!-- Divider -->
      <span class="divider">/</span>
      <a href="#" class="bread-current">ProductCategory</a>
    </div>

    <div class="clearfix"></div>

    </div>
    <!-- Page heading ends -->
    <div class="matter">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <div class="widget-head">
                            <div class="pull-left">
                                <a href="{{ Url::to('master/product/create') }}" class="btn btn-info btn-xs">Create</a>
                            </div>
                          <div class="widget-icons pull-right">
                            <a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a>
                            <a class="wclose" href="#"><i class="fa fa-times"></i></a>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="padd">
                                <div class="page-tables">
                                    <div class="table-responsive">
                                        {{ Datatable::table()
                                            ->addColumn('id', 'Parent Id', 'Name')       // these are the column headings to be shown
                                            ->setUrl(route('api.category'))   // this is the route where data will be retrieved
                                            ->render()
                                        }}
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget-foot">
                            <div class="pull-left">
                                <a href="{{ Url::to('master/category/create') }}" class="btn btn-info btn-sm">Create</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- matter ends -->
@stop