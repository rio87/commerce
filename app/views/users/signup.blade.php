<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>Aplikasi Toko</title>
            <meta name="description" content="">
            <meta name="author" content="">

            <!-- Mobile Specific Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

            <!-- Stylesheets -->
            <!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">-->
            {{ HTML::style('/public/css/bootstrap.min.css') }}
            {{ HTML::style('/public/css/font-awesome.min.css') }}
            {{ HTML::style('/public/css/jquery-ui.css') }}
            {{ HTML::style('/public/css/style.css') }}
            {{ HTML::style('/public/css/widgets.css') }}

            {{ HTML::script('/js/respond.min.js') }}
            <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
    </head>
    <body>
        <!-- Form area -->
        <div class="admin-form">
          <div class="container">

            <div class="row">
              <div class="col-md-12">
                <!-- Widget starts -->
                    <div class="widget worange">
                      <!-- Widget head -->
                      <div class="widget-head">
                        <i class="fa fa-lock"></i> Signup
                      </div>

                      <div class="widget-content">
                        <div class="padd">
                          {{ Confide::makeSignupForm()->render(); }}
        				</div>
                      </div>

                    </div>
              </div>
            </div>
          </div>
        </div>

    </body>
</html>