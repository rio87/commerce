<?php

function set_active($path, $active='open')
{
    return Request::is($path) || Request::is($path . '/*') ? $active: '';
}

function set_current($path, $active='current')
{
    return Request::is($path) || Request::is($path . '/*') ? $active: '';
}

/**
 * function for tree menu
 * @param array $data
 * @param int $parent
 * @return array
 */
function buildTree(Array $data, $parent = 0) {
    $tree = array();
    foreach ($data as $d) {
        if ($d['parent_id'] == $parent) {
            $children = buildTree($data, $d['id']);
            // set a trivial key
            if (!empty($children)) {
                $d['_children'] = $children;
            }
            $tree[] = $d;
        }
    }
    return $tree;
}

/**
 * @param $tree
 * @param int $r
 * @param null $p
 */
function printTree($tree, $r = 0, $p = null) {
    foreach ($tree as $i => $t) {
        $dash = ($t['parent_id'] == 0) ? '' : str_repeat('-', $r) .' ';
        printf("\t<option value='%d'>%s%s</option>\n", $t['id'], $dash, $t['name']);

        if (isset($t['_children'])) {
            printTree($t['_children'], $r+1, $t['parent_id']);
        }
    }
}

function getPropinsi()
{
    $propinsi = Propinsi::All();

    return $propinsi;
}