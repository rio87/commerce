<?php
/**
 * Created by PhpStorm.
 * User: rio
 * Date: 28/10/14
 * Time: 13:51
 */

class StoreController extends BaseController {

    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    /**
     * untuk display halaman utama dari toko
     */
    public function getIndex()
    {
        return View::make('frontend.home')->with('product', Product::take(12)->where(['status' => '1'])->orderBy('created_at', 'DESC')->get());
    }

    /**
     * @param $parent_id
     * @return mixed
     */
    public function getItems($parent_id)
    {
        $items = Category::tree($parent_id);
        //$products = CategoryProduct::where('status', '=', '1')->orderBy('created_at', 'DESC')->paginate(9);
        $products = DB::table('category_product')
            ->join('product', 'category_product.id_product', '=', 'product.id')
            ->join('category', 'category_product.id_category', '=', 'category.id')
            ->join('product_image', 'product.image_id', '=', 'product_image.id')
            ->where('category.slug', '=', $parent_id)
            ->select('product.*', 'product_image.name as img_name')
            ->orderBy('product.created_at', 'DESC')->paginate(9);
        $Category = Category::where('slug','=',$parent_id)->get();
        return View::make('frontend.items')->with(['items' => $items, 'products' => $products, 'category' => $Category]);
    }

    /**
     * @param $category_id
     * @param $parent_id
     * @return mixed
     */
    public function getItemsDetil($category_id, $parent_id)
    {
        $items = Category::tree($parent_id);
        $products = Product::where('status', '=', '1')->where('category_id', '=', $category_id)->orderBy('created_at', 'DESC')->paginate(9);
        $Category = Category::where('id','=',$category_id)->get();
        return View::make('frontend.itemDetil')->with(['items' => $items, 'products' => $products, 'category_id' => $Category]);
    }

    public function getItemsView($id)
    {
        $items = Category::tree(0);
        $products = Product::where('status', '=', '1')->where('slug', '=', $id)->firstOrFail();

        return View::make('frontend.itemView')->with(['products' => $products, 'items' => $items]);
    }

    public function postAddtocart()
    {
        $product = Product::find(Input::get('id'));
        $quantity = Input::get('quantity');

        Cart::add(
          [
           'id'      => $product->id,
           'name'    => $product->name,
           'qty'     => $quantity,
           'price'   => $product->price,
          ]
        );

        return Redirect::to('store/cart');
    }

    public function getCart()
    {
        return View::make('frontend.cart')->with('products', Cart::content());
    }

    public function delCart()
    {
        Cart::remove(Input::get('id'));
        return Redirect::to('store/cart');
    }
}