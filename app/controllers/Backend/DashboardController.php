<?php
/**
 * Created by PhpStorm.
 * User: rio
 * Date: 23/10/14
 * Time: 17:07
 */

class DashboardController extends Controller {
    /**
     * index dashboard
     */
    public function index()
    {
        return View::make('backend.dashboard');
    }
}