<?php


class ProductController extends \BaseController {

	private $Product;

	private $Category;

	private $CategoryProduct;

	private $image;

	public function __construct(Product $product, Category $Category, CategoryProduct $categoryProduct, ProductImage $image)
	{
		$this->beforeFilter('csrf', ['on' => ['post']]);
		$this->Product = $product;
		$this->Category = $Category;
		$this->CategoryProduct = $categoryProduct;
		$this->image = $image;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$product = $this->Product->paginate(10);
		return View::make('backend.master.product.index')->with(['product' => $product]);
	}

	public function getDataTable(){

		$query = Product::all();

		return Datatable::collection($query)
			->showColumns('id', 'category', 'name', 'description', 'price', 'in_stock', 'status', 'image', 'action')
			->addColumn('category', function($model){
				return $model->category->name;
			})
			->addColumn('status', function($model){
				return ($model->status) == '1' ? 'Publish' : 'Unpublish';
			})
			->addColumn('image', function($model){
				$image = $model->image->name != '' ? $model->image->name : 'img/img-not-available.jpg';
				return HTML::image('/public/back/'.$image, null, ['class' => 'img-responsive', 'height' => '100',  'width' => '100']);
			})
			->addColumn('action', function($model){
				return "<a href='".URL::to('master/product/'.$model->id.'/edit')."' class='btn btn-xs btn-warning'><span class='fa fa-pencil'></span></a>";
			})
			->searchColumns('name')
			->orderColumns('name')
			->make();
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$category = $this->Category->all();
		$buildTree = buildTree($category->toArray());
		return View::make('backend.master.product.create')->with(['buildTree' => $buildTree, 'model' => $this->Product]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make( Input::all(), Product::$rules ); # Or User::$rules['create']

		if ( $validator->fails() ) {
			$messages = $validator->messages();

			return Redirect::to('master/product/create')
				->withErrors($validator);
		}else {
			$image = Input::file('image_id');
			$filename = 	$image->getClientOriginalName();
			$path = public_path('back/img/products/' . $filename);
			Image::make($image->getRealPath())->resize(800, 762)->save($path);

			$upload = $this->image->create([
				'name'	=> 'img/products/'.$filename
			]);

			$category_id1 = Input::get('category_id');

			foreach ($category_id1 as $key1 => $value1) {

			}

			$product = $this->Product->create([
				'category_id' => $value1,
				'sku' => Input::get('sku'),
				'name' => Input::get('name'),
				'description' => Input::get('description'),
				'price' => Input::get('price'),
				'size' => Input::get('size'),
				'in_stock' => Input::get('in_stock'),
				'status' => Input::get('status'),
				'image_id' => $upload->id
			]);

			$category_id = Input::get('category_id');

			foreach ($category_id as $key => $value) {
				$this->CategoryProduct->create([
					'id_category' => $value,
					'id_product' => $product->id
				]);
			}

			return Redirect::to('master/product');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$model = $this->Product->find($id);
		$category = $this->Category->all();
		$buildTree = buildTree($category->toArray());
		return View::make('backend.master.product.edit')->with(['model' => $model, 'buildTree' => $buildTree]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$product = $this->Product->find($id);
		$upload = $this->image->find($product->image_id);


		$validator = Validator::make( Input::all(), Product::$rules ); # Or User::$rules['create']

		if ( $validator->fails() ) {
			$messages = $validator->messages();

			return Redirect::to('master/product/'.$product->id.'/edit')
				->withErrors($validator);
		}else {
			$image = Input::file('image_id');

			if($image != null):
				$filename = $image->getClientOriginalName();
				$path = public_path('back/img/products/' . $filename);
				Image::make($image->getRealPath())->resize(800, 762)->save($path);
				$upload->name = 'img/products/' . $filename;
				$upload->save();
			else:
				$filename = $upload->name;
				$upload->name = $filename;
				$upload->save();
			endif;

			//$product->category_id = Input::get('category_id');
			$product->sku = Input::get('sku');
			$product->name = Input::get('name');
			$product->description = Input::get('description');
			$product->price = Input::get('price');
			$product->size = Input::get('size');
			$product->in_stock = Input::get('in_stock');
			$product->status = Input::get('status');
			$product->image_id = $upload->id;
			$product->save();

			$category_id = Input::get('category_id');
			$category_product = $this->CategoryProduct->where('id_product', '=', $id)->delete();
			foreach ($category_id as $key => $value) {
				$this->CategoryProduct->create([
					'id_category' => $value,
					'id_product' => $product->id
				]);
			}

			return Redirect::to('master/product');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	private function getCategories()
	{
		$categories = array();

		foreach ($this->Category->where('parent_id', '<>', 0)->get() as $category) {
			$categories[$category->id] = $category->name;
		}

		return $categories;
	}
}
