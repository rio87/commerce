<?php

class CategoryController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$category = Category::paginate(10);
		return View::make('backend.master.product_category.index')->with(['category' => $category]);
	}

	public function getDataTable()
	{
		return Datatable::collection(Category::all())
			->showColumns('id', 'parent_id', 'name')
			->searchColumns('name')
			->orderColumns('id')
			->make();
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$category = Category::all();
		$buildTree = buildTree($category->toArray());

		return View::make('backend.master.product_category.create')->with(['buildTree' => $buildTree]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validator = Validator::make($input, Category::$rules);

		if ( $validator->fails() ) {
			$messages = $validator->messages();

			return Redirect::to('master/category/create')
				->withErrors($validator);
		}else{
			Category::create($input);

			return Redirect::to('master/category');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
