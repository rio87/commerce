<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('sku', 60);
			$table->string('name', 100);
			$table->text('description');
			$table->double('price');
			$table->double('price_discount');
			$table->string('size', 20);
			$table->integer('in_stock');
			$table->smallInteger('status');
			$table->integer('category_id')->unsigned();
			$table->foreign('category_id')->references('id')->on('product_category');
			$table->integer('image_id')->unsigned();
			$table->foreign('image_id')->references('id')->on('product_image');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product');
	}

}
