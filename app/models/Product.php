<?php
/**
 * Created by PhpStorm.
 * User: rio
 * Date: 23/10/14
 * Time: 16:31
 */
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Product extends Eloquent implements SluggableInterface
{
    use SluggableTrait;

    protected $table = 'product';

    protected $fillable = ['category_id', 'sku', 'name', 'description', 'price', 'size', 'in_stock', 'status', 'image_id'];

    protected $sluggable = [
      'build_from' => 'name',
      'save_to' => 'slug'
    ];

    public static $rules = [
        'sku' => 'required',
        'name' => 'required',
        'category_id' => 'required',
        'description' => 'required',
        'price' => 'required',
        'size' => 'required',
        'in_stock' => 'required',
        'status' => 'required',
        'image_id' => 'image|mimes:jpeg,jpg,bmp,png,gif',
    ];

    public function category(){
        return $this->belongsTo('Category');
    }

    public function image(){
        return $this->belongsTo('ProductImage');
    }
}