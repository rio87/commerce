<?php
/**
 * Created by PhpStorm.
 * User: rio
 * Date: 23/10/14
 * Time: 17:46
 */
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Category extends Eloquent implements SluggableInterface{
    use SluggableTrait;

    protected $table = 'category';

    protected $fillable = ['parent_id', 'name'];

    public static $rules = [
        'name' => 'required',
    ];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug'
    ];

    public function products(){
        return $this->hasMany('Product');
    }

    public function parent() {

        return $this->hasOne('Category', 'id', 'parent_id');

    }

    public function children() {

        return $this->hasMany('Category', 'parent_id', 'id');

    }

    public static function tree($parent_id) {
        if(is_integer($parent_id)){
            $condition = 'parent_id';
        }elseif(is_string($parent_id)){
            $condition = 'slug';
        }
        return static::with(implode('.', array_fill(0, 100, 'children')))->where($condition, '=', $parent_id)->get();

    }

}