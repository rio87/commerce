<?php
/**
 * Created by PhpStorm.
 * User: rio
 * Date: 14/11/14
 * Time: 16:52
 */

class CategoryProduct extends Eloquent{
    protected $table = 'category_product';

    protected $fillable = ['id_category', 'id_product'];

    public function category()
    {
        return $this->hasMany('Category');
    }

    public function product()
    {
        return $this->hasMany('Product');
    }
}