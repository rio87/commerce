<?php
/**
 * Created by PhpStorm.
 * User: rio
 * Date: 23/10/14
 * Time: 18:17
 */

class ProductImage extends Eloquent{
    protected $table = 'product_image';

    protected $fillable = ['name'];

    public function products(){
        return $this->hasMany('Product');
    }
}